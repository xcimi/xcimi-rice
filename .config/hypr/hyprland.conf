# This is an example Hyprland config file.
# Syntax is the same as in Hypr, but settings might differ.
#
# Refer to the wiki for more information.

monitor=,1920x1080@60,0x0,1
workspace=DP-1,1

#startup
exec-once=dunst
exec-once=hyprpaper
exec-once=waybar -c .config/waybar/config.json -s .config/waybar/style.css
exec-once=discord
exec-once=kitty
exec-once=firefox
exec-once=clight
exec-once=exec /usr/lib/xfce-polkit/xfce-polkit
exec-once=wl-copy
exec-once=wl-paste

input {
    kb_layout=
    kb_variant=
    kb_model=
    kb_options=
    kb_rules=
    force_no_accel=1
    follow_mouse=1
    touchpad {
        natural_scroll=1
	}
    }



general {
    max_fps=60 # deprecated, unused
    sensitivity=1
    main_mod=SUPER

    gaps_in=5
    gaps_out=8
    border_size=1
    col.active_border=0xff00ff00
    col.inactive_border=0xffffff00

    apply_sens_to_raw=1 
    damage_tracking=full # leave it on full unless you hate your GPU and want to make it suffer
}

decoration {
    rounding=8
    multisample_edges=1
    blur=0
    blur_size=3 # minimum 1
    blur_passes=1 # minimum 1, more passes = more resource intensive.
    # Your blur "amount" is blur_size * blur_passes, but high blur_size (over around 5-ish) will produce artifacts.
    # if you want heavy blur, you need to up the blur_passes.
    # the more passes, the more you can up the blur_size without noticing artifacts.
}

animations {
    enabled=1
    animation=windows,1,3,overshot,slide
    animation=borders,1,60,undershot,default
    animation=fadein,1,3,careful,default
    animation=workspaces,1,3,undershot,slidevert
}

curves {
    bezier=careful,0.5,1.0,0.8,1.0
    bezier=overshot,0.5,1.6,0.8,1.0
    bezier=undershot,0.0,1.0,0.0,1.0
}

dwindle {
    pseudotile=1 # enable pseudotiling on dwindle
    preserve_split=1
}

#window rules
windowrule=float,Rofi

#binds
bind=SUPER,Q,exec,firefox
bind=SUPER,W,killactive,
bind=SUPER,V,togglefloating,
bind=SUPER,S,exec,rofi -show drun
bind=SUPER,P,pseudo,
bind=SUPER,T,exec,kitty
bind=SUPERSHIFT,S,exec,grim -g "$(slurp)" - | swappy -f -
bind=SUPER,L,exec,swaylock -c 000000 --font "Noto Sans Mono" --font-size 16 --ring-color 5cffff 

bind=SUPER,left,movefocus,l
bind=SUPER,right,movefocus,r
bind=SUPER,up,movefocus,u
bind=SUPER,down,movefocus,d
bind=SUPERALT,escape,exec,hyprctl kill
bind=SUPER,1,workspace,1
bind=SUPER,2,workspace,2
bind=SUPER,3,workspace,3
bind=SUPER,4,workspace,4
bind=SUPER,5,workspace,5
bind=SUPER,6,workspace,6
bind=SUPER,7,workspace,7
bind=SUPER,8,workspace,8
bind=SUPER,9,workspace,9
bind=SUPER,0,workspace,10
bind=SUPER,minus,workspace,-1
bind=SUPER,equal,workspace,+1

bind=SUPERALT,1,movetoworkspacesilent,1
bind=SUPERALT,2,movetoworkspacesilent,2
bind=SUPERALT,3,movetoworkspacesilent,3
bind=SUPERALT,4,movetoworkspacesilent,4
bind=SUPERALT,5,movetoworkspacesilent,5
bind=SUPERALT,6,movetoworkspacesilent,6
bind=SUPERALT,7,movetoworkspacesilent,7
bind=SUPERALT,8,movetoworkspacesilent,8
bind=SUPERALT,9,movetoworkspacesilent,9
bind=SUPERALT,0,movetoworkspacesilent,10
bind=SUPERALT,minus,movetoworkspacesilent,-1
bind=SUPERALT,equal,movetoworkspacesilent,+1
